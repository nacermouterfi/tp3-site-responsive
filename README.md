
# TP3 -SITE_WEB_RESPONSIVE
Ce travail consiste en la réalisation et intégration d'un **Site web responsive** de quatre (4) pages plus une autre cachée intitulé : **Confirmation de votre réservation** sur le thème **Restaurant**.

## MISE EN CONTEXTE : 
### Pages de ce site : 
1. [Accueil](#acceuil)
2. [Réservation](#reservation)
3. [Restaurant](#restaurant)
4. [Ma commande](#macommande)
5. [Confirmation-réservation](#confirmation) // Page cachée avec un lien depuis la page **Ma commande**.

### Objectifs pédagogiques mis en oeuvre : 

* [Versionning **GIT**](https://gitlab.com/nacermouterfi/tp3-site-responsive) / gitlab
* Utilisation des _media queries_ pour le responsive.
* Intégration d'un **formulaire** dans la page **Réservation** (Détail ci-dessous).
* Interprétation et respect des _wireframes_ (**maquettes filaires**) fournies.
* Usages appropriés et appropriés des concepts **HTML** et **CSS** (_flexbox_ et _Css-Grid_).

## MÉTHODES DE TRAVAIL : 
1 - PAGE ACCUEIL (_index.html_) est réparties en quatre (4) blocs : 
- **Bloc 1** : Header : Navigation ( _Logo_ - **Accueil** - **Réservation** - **Restaurant** - **Ma Commande** ).
- **Bloc 2** : Page de garde : _Le texte portant le nom du restaurant avec_ ```h1```, une courte **description** avec deux liens : **button** _Commander en ligne_ et _Réserver_
- **Bloc 3** : _**Temoignages**_ en ```h2```, et le sous-titre _**Clients satisfaits**_ en ```h3```, fait en ```Flexbox```.
<<<<<<< HEAD
- **Bloc 4** : 
=======
- **Bloc 4** : **Footer** j'ai utilisé le ```CSS Grid``` pour répartir les quatre _fractions_ utilisées : _Qui sommes-nous_, _Plat_, _Ouverture_ et _Localisation_. Sous ce bloc, j'ai crée la partie en ligne de mon identification : TP3_Nacer_Mouterfi_Site-Web-Responsive, Cégep Trois-Rivieres et Session.
>>>>>>> 8c6d095e45f8888f2ad24a3b8c01f5839a3bb3ca

Les fonts utilisées :
@import url('https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,500;1,700;1,900&display=swap');
<<<<<<< HEAD
@import url('https://fonts.googleapis.com/css2?family=Cormorant+Garamond:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap');
=======